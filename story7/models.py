from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    desc = models.TextField(max_length=2000, blank=True, null=True)

class Kepanitiaan(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    date = models.CharField(max_length=100, blank=True, null=True)
    position = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(max_length=2000, blank=True, null=True)

class Organisasi(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    date = models.CharField(max_length=100, blank=True, null=True)
    position = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(max_length=2000, blank=True, null=True)