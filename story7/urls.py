from django.urls import path
from story7.views import story7_page

app_name = "story7"

urlpatterns = [
    path("", story7_page, name="story7"),
]
