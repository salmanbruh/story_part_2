from django.shortcuts import render
from story7.models import Kegiatan, Kepanitiaan, Organisasi

# Create your views here.

def story7_page(request):
    kegiatan = Kegiatan.objects.all()
    kepanitiaan = Kepanitiaan.objects.all()
    organisasi = Organisasi.objects.all()
    context = {
        'title':'Story 7',
        'kegiatan':kegiatan,
        'kepanitiaan':kepanitiaan,
        'organisasi':organisasi,
    }
    return render(request, "story7/index.html", context)