from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story7.models import Kegiatan, Organisasi, Kepanitiaan
from story7.views import story7_page

# Create your tests here.

class Story7UnitTest(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get("/story7/")
        self.assertEqual(response.status_code, 200)

    def test_story7_url_using_index_function(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7_page)

    def test_model_can_create_new_kegiatan(self):
        new_kegiatan = Kegiatan.objects.create(
            name = "Nothing",
            desc = "Do nothing"
        )
        count_all_available_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(count_all_available_kegiatan, 1)

    def test_model_can_create_new_organisasi(self):
        new_organisasi = Organisasi.objects.create(
            name = "Asedehe",
            date = "August - November 2019",
            position = "Staff - Santuy Division",
            description = "Did basically nothing"
        )
        count_all_available_organisasi = Organisasi.objects.all().count()
        self.assertEqual(count_all_available_organisasi, 1)

    def test_model_can_create_new_kepanitiaan(self):
        new_kepanitiaan = Kepanitiaan.objects.create(
            name = "Asedehe a dehe",
            date = "October - November 2019",
            position = "Manager - Santuy Division",
            description = "Helping people do nothing"
        )
        count_all_available_kepanitiaan = Kepanitiaan.objects.all().count()
        self.assertEqual(count_all_available_kepanitiaan, 1)

class Story7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_change_theme_color(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/story7/")
        
        toggler = selenium.find_element_by_id('toggle-button')
        background = selenium.find_element_by_tag_name('body')

        #Check theme color before change
        self.assertEqual("rgba(255, 255, 255, 1)", background.value_of_css_property("background-color"))

        #change theme color
        toggler.click()

        #check theme color after change
        self.assertEqual("rgba(22, 22, 37, 1)", background.value_of_css_property("background-color"))

    def test_accordion(self):
        #define selenium
        selenium = self.selenium

        #create kegiatan, kepanitiaan, organisasi
        new_kegiatan = Kegiatan.objects.create(
            name = "Nothing",
            desc = "Do nothing"
        )
        new_kepanitiaan = Kepanitiaan.objects.create(
            name = "Asedehe a dehe",
            date = "October - November 2019",
            position = "Manager - Santuy Division",
            description = "Helping people do nothing"
        )
        new_organisasi = Organisasi.objects.create(
            name = "Asedehe",
            date = "August - November 2019",
            position = "Staff - Santuy Division",
            description = "Did basically nothing"
        )

        #getting page
        selenium.get("http://127.0.0.1:8000/story7/")

        #check if header exist
        self.assertIn("Kegiatan", selenium.page_source)
        self.assertIn("Kepanitiaan", selenium.page_source)
        self.assertIn("Organisasi", selenium.page_source)

        #check if header is not open
        self.assertNotIn("is-open", selenium.page_source)

        #search for elements to click to show accordion
        kegiatan_header = selenium.find_element_by_id("kegiatan_header")
        
        #Check inner kegiatan
        kegiatan_header.click()
        self.assertIn("is-open", selenium.page_source)
