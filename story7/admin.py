from django.contrib import admin
from story7.models import Kegiatan, Organisasi, Kepanitiaan
# Register your models here.

admin.site.register(Kegiatan)
admin.site.register(Organisasi)
admin.site.register(Kepanitiaan)
