from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from story9.forms import SignUpForm

# Create your views here.

def story9_page(request):
    context = {
        'title':'Story 9',
    }
    return render(request, "story9/index9.html", context)

def story9_login_page(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            user = form.get_user()

            if user is not None:
                login(request, user)
                return redirect('/story9/')
            else:
                return redirect('/story9/login/')
    else:
        form = AuthenticationForm()
        
    context = {
        'title':'Story 9 Login',
        'form':form,
    }

    return render(request, "story9/login.html", context)

def story9_logout_page(request):
    context = {
        'title':'Logout'
    }

    if request.method == "POST":
        if request.POST["logout"] == 'Logout':
            logout(request)
            return redirect("/story9/")

    return render(request, "story9/logout.html", context)

def story9_signup_page(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/story9/')
    else:
        form = SignUpForm()
    return render(request, 'story9/signup.html', {'form': form})