from django.urls import path
from story9.views import story9_page, story9_login_page, story9_logout_page, story9_signup_page

app_name = "story9"

urlpatterns = [
    path("", story9_page, name="story9"),
    path("login/", story9_login_page, name="story9_login"),
    path("logout/", story9_logout_page, name="story9_logout"),
    path("signup/", story9_signup_page, name="story9_signup")
]
