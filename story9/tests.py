from django.test import TestCase
from django.test import TestCase
from django.test import TestCase, Client
import time
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from story9.views import story9_page, story9_login_page, story9_logout_page, story9_signup_page

class Story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        response = Client().get("/story9/")
        self.assertEqual(response.status_code, 200)

    def test_story9_url_using_index_function(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9_page)
    
    def test_story9Login_url_is_exist(self):
        response = Client().get("/story9/login/")
        self.assertEqual(response.status_code, 200)

    def test_story9Login_url_using_index_function(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, story9_login_page)

    def test_story9logout_url_is_exist(self):
        response = Client().get("/story9/logout/")
        self.assertEqual(response.status_code, 200)

    def test_story9logout_url_using_index_function(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, story9_logout_page)
    
    def test_story9signup_url_is_exist(self):
        response = Client().get("/story9/signup/")
        self.assertEqual(response.status_code, 200)

    def test_story9signup_url_using_index_function(self):
        found = resolve('/story9/signup/')
        self.assertEqual(found.func, story9_signup_page)

    def test__session_id_exist_when_logged_in(self):
        user = User.objects.create_user('test username', 'mail@mail.com', 'test password')
        user.first_name = 'Test'
        user.save()

        response = Client().post('/story9/login/', data={'username':'test username', 'password':'test password'})
        self.assertEqual(response.status_code, 302)

    def test_can_save_user_registration(self):
        count = User.objects.all().count()
        self.assertEqual(count, 0)

        data = {'username':'sangatganteng','email':'a@mail.com', 'password1':'cxvlrskemglkjserg', 'password2':'cxvlrskemglkjserg'}
        response = Client().post('/story9/signup/', data)
        count = User.objects.all().count()
        self.assertEqual(count, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story9/')

        new_response = Client().get('/story9/')
        self.assertTemplateUsed(new_response, 'story9/index9.html')