from django.urls import path
from story8.views import story8_page

app_name = "story8"

urlpatterns = [
    path("", story8_page, name="story8"),
]
