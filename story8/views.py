from django.shortcuts import render

# Create your views here.

def story8_page(request):
    context = {
        'title':'Story 8',
    }
    return render(request, "story8/index.html", context)