from django.test import TestCase
from django.test import TestCase, Client
import time
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story8.views import story8_page

class Story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get("/story8/")
        self.assertEqual(response.status_code, 200)

    def test_story8_url_using_index_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8_page)

class Story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_searched_book_exist(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/story8/")

        search_box = selenium.find_element_by_class_name("search-box")
        search_button = selenium.find_element_by_class_name("search-button")

        time.sleep(2)
        search_box.send_keys("web")
        time.sleep(2)
        search_button.click()

        time.sleep(2)
        self.assertIn("Hello Web App", selenium.page_source)
