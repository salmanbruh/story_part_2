$(document).ready(function () {
    $(".search-box").val()
    $.ajax({
        async: true,
        type: "GET",
        url: "https://www.googleapis.com/books/v1/volumes?q=naruto",
        dataType: "json",
        success: function (response) {
            let bookList = response.items;
            let bookListLength = response.totalItems;
            $("table").removeClass("no-books")
            $("h4").removeClass("no-books")
            $("h4").append("Search results for keyword : naruto")
            $("tbody").empty();

            for (let index = 0; index < bookListLength; index++) {
                let book = bookList[index].volumeInfo;
                var num = $("<td>").text(index + 1);

                if ('imageLinks' in book == false)
                    var cover = $('<td>').text("-");
                else {
                    if ('smallThumbnail' in book.imageLinks == false)
                        var cover = $('<td>').append($('<img>').attr({
                            'src': book.imageLinks.thumbnail
                        }));
                    else
                        var cover = $('<td>').append($('<img>').attr({
                            'src': book.imageLinks.smallThumbnail
                        }));
                }
                
                var name = $("<td>").text(book.title)

                if ('authors' in book == false) var author = $('<td>').text("-");
                else var author = $('<td>').text(book.authors);

                if ('publisher' in book == false) var publisher = $('<td>').text("-");
                else var publisher = $('<td>').text(book.publisher);

                if ('pageCount' in book == false) var pageCount = $('<td>').text("-");
                else var pageCount = $('<td>').text(book.pageCount);

                var newRow = $("<tr>").append(num, cover, name, author, publisher, pageCount);
                $("tbody").append(newRow);

                $("tr").children().addClass("align-middle")
            }
        },
        error : function(){
            alert("Internal error occurred. Please reload the page.")
        },
        type : 'GET',
    });

    $(".search").submit(function (e) { 
        let keyword = $(".search-box").val()
        let keywordURL = "https://www.googleapis.com/books/v1/volumes?q=" + keyword

        $.ajax({
            async: true,
            type: "GET",
            url: keywordURL,
            dataType: "json",
            success: function (response) {
                let bookList = response.items;
                let bookListLength = response.totalItems;
                $("table").removeClass("no-books")
                $("h4").removeClass("no-books")
                $("h4").empty()
                $("h4").append("Search results for keyword : " + keyword)
                $("tbody").empty()

                for (let index = 0; index < bookListLength; index++) {
                    let book = bookList[index].volumeInfo;
                    var num = $("<td>").text(index + 1);

                    if ('imageLinks' in book == false)
                        var cover = $('<td>').text("-");
                    else {
                        if ('smallThumbnail' in book.imageLinks == false)
                            var cover = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var cover = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }
                    
                    var name = $("<td>").text(book.title)

                    if ('authors' in book == false) var author = $('<td>').text("-");
                    else var author = $('<td>').text(book.authors);

                    if ('publisher' in book == false) var publisher = $('<td>').text("-");
                    else var publisher = $('<td>').text(book.publisher);

                    if ('pageCount' in book == false) var pageCount = $('<td>').text("-");
                    else var pageCount = $('<td>').text(book.pageCount);

                    var newRow = $("<tr>").append(num, cover, name, author, publisher, pageCount);
                    $("tbody").append(newRow);

                    $("tr").children().addClass("align-middle")
                }
            },
            error : function(){
                alert("Internal error occurred. Please reload the page.")
            },
            type : 'GET',
        });
        e.preventDefault();
    });
});