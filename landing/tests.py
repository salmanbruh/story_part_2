from django.test import TestCase, Client
from django.urls import resolve
from .views import landing_page
from .models import Status
from .forms import Status_Form
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class Story6UnitTest(TestCase):
    def test_redirect_to_landing(self):
        reponse = Client().get('//')
        self.assertEqual(reponse.status_code, 302)

    def test_landing_url_is_exist(self):
        reponse = Client().get('/landing/')
        self.assertEqual(reponse.status_code, 200)

    def test_landing_using_index_func(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, landing_page)

    def test_model_can_create_new_status(self):
        #Creating new Status
        new_status = Status.objects.create(status="Aman sentosa, santuy bae.")

        #Retrieve all available Status
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn("status-input", form.as_p())
        self.assertIn('for="status-input"', form.as_p())

    def test_form_validation_for_blank_items(self):
        date_created = datetime.now
        form = Status_Form(data={'status':'', 'date_created':date_created})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_landing_post_success_and_render_the_result(self):
        status_test = "Aman jaya, santuy bae"
        response_post = self.client.post(('/landing/'), {'status':status_test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/landing/')
        html_response = response.content.decode('utf8')
        self.assertIn(status_test, html_response)

    def test_landing_post_error_and_render_the_result(self):
        status_test = "Aman jaya, santuy bae"
        date_created = datetime.now
        response_post = Client().post('/landing/', {'status': status_test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/landing/')
        html_response = response.content.decode('utf8')
        self.assertIn(status_test, html_response)

class Story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium

        #Open link for testing
        selenium.get('http://127.0.0.1:8000/landing/')

        #find form element
        status = selenium.find_element_by_id('status-input')
        submit = selenium.find_element_by_id('submit-button')

        #Fill form with data
        status.send_keys("Mantap coeg, santuy bae")

        #Submit form
        submit.send_keys(Keys.RETURN)

        #unittest
        self.assertIn("Mantap", selenium.page_source)