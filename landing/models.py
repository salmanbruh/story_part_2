from django.db import models
from datetime import datetime

# Create your models here.
class Status(models.Model):
    status = models.TextField(max_length=300, blank=False, null=False)
    date_created = models.DateTimeField(default=datetime.now, blank=False)