from django.shortcuts import render, redirect
from landing.models import Status
from landing.forms import Status_Form

# Create your views here.

def index_page(request):
    return redirect('/landing/')

def landing_page(request):
    form = Status_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('/landing/')

    statuses = Status.objects.all()
    context = {
        'page_title':'landing',
        'status':statuses,
        'form':form,
    }
    return render(request, 'landing/landing.html', context)