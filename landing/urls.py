from django.urls import path
from landing.views import landing_page, index_page

app_name = "landing"

urlpatterns = [
    path("", index_page, name="index"),
    path("landing/", landing_page, name="landing"),
]
